package com.example.user.androidbikecomputer;

import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.Calendar;


public class MainActivity extends AppCompatActivity implements LocationListener {
    private Handler myHandler;
    private LocationManager locationManager;
    private Button mButtonHIT;
    private long mHIT;
    private Runnable myRunnable = new Runnable() {
        @Override
        public void run() {

            String timeStamp = new SimpleDateFormat("HH:mm").format(Calendar.getInstance().getTime());

            TextView title = (TextView) findViewById(R.id.timeView);
            title.setText(timeStamp);
            myHandler.postDelayed(this,500);

            long t = System.currentTimeMillis();

            if(( t - mHIT) < 20000){
                mButtonHIT.setBackgroundColor(0xffff0000);
                mButtonHIT.setText(String.format("%d",(t-mHIT)/1000));

            } else if(( t - mHIT) < 60000){
                mButtonHIT.setBackgroundColor(0xff101010);
                mButtonHIT.setText(String.format("%d",(t-mHIT)/1000-20));
            } else {
                mHIT = t;
            }
         }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        String timeStamp = new SimpleDateFormat("HH:mm").format(Calendar.getInstance().getTime());

        TextView title = (TextView) findViewById(R.id.timeView);
        title.setText(timeStamp);

        mButtonHIT = (Button) findViewById(R.id.buttonHIT);
        mHIT =  System.currentTimeMillis();
        mButtonHIT.setOnClickListener( new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                mHIT =  System.currentTimeMillis();
            }
        });

        myHandler = new Handler();
        myHandler.postDelayed(myRunnable,5000);

        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);

        this.checkCallingPermission(android.Manifest.permission.ACCESS_FINE_LOCATION);

        locationManager.requestLocationUpdates( LocationManager.GPS_PROVIDER,
        1000,   // 3 sec
        0,
        this);



    }


    @Override
    public void onLocationChanged(Location location) {
        TextView title = (TextView) findViewById(R.id.speedView);
        title.setText( String.format("%2.1f",location.getSpeed()*3.6f));
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }
}
